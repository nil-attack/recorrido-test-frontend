
# README

### Requisitos:
Para ejecutar el proyecto se necesita `docker` y `docker-compose`

### Instalacion
Construimos el Docker
`docker-compose build`
Instalamos dependencias
`docker-compose run --rm web npm install`
Ejecutamos nuestra app (se abre en 0.0.0.0:4200)
`docker-compose run --rm --service-ports web`

Nota: El app se conecta a (0.0.0.0:80, dado que asume que el docker del backend se ejecuta alli)
