import { Component, Inject, OnInit } from '@angular/core';
import { TripPathsService } from "../trip-paths.service";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { map } from "rxjs/operators";
import { from, Observable, pipe } from "rxjs";
import { DatePipe } from '@angular/common';
import { FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-trip-details',
  templateUrl: './trip-details.component.html',
  styleUrls: ['./trip-details.component.scss']
})
export class TripDetailsComponent implements OnInit {

  trip_id;
  displayedColumns = ['message'];
  events: Observable<any> = from([])
  coordsForm;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private tripPathsService: TripPathsService,
              private datePipe: DatePipe, private formBuilder: FormBuilder) {
    this.trip_id = data["trip_id"]
    this.getRouteEntranceAndExitEvents()
    this.createForm();
  }

  createForm() {
    this.coordsForm = this.formBuilder.group({
      coordinates: ['', Validators.required]
    });
  }

  getRouteEntranceAndExitEvents() {
    this.events = this.tripPathsService
                      .getEntranceAndExitRouteEvents(this.trip_id)
      .pipe(map((ev: any[]) => {
        return ev["points"].map(pt => {
          const date = this.datePipe.transform(pt["created_at"], 'shortTime');
          const [lon, lat] = pt["lonlat"].replace("POINT (", '').replace(")", '').split(" ");

          if (pt["status"] == 'within route') {
            return {message: `${date} entró a la ruta en ${lon}, ${lat}`};
          }
          return {message: `${date} salió de la ruta en ${lon}, ${lat}`}
        })
      }));
  }

  ngOnInit() {
  }

  onSubmit() {
    const [lon, lat] = this.coordsForm.value.coordinates.split(" ");
    this.tripPathsService.getByCoordinates(this.trip_id,lat, lon)
        .subscribe(resp => {

          if (resp == null) {
            return alert('No pasó por esta coordenada')
          }
          const date = this.datePipe.transform(resp["result"]["started_at"], 'shortTime');
          if (resp["result"]["seconds_spent"] >= 120) {

            return alert(`Se detuvo en el lugar por ${resp["result"]["seconds_spent"] / 60} minutos a las ${date}`);
          }
          return alert(`Pasó por esta coordenada a las ${date} pero no se detuvo`)
        })

  }
}
