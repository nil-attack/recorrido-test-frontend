import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RoutesService {
  private ROUTES_URI = 'http://0.0.0.0/api/v1/routes';
  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get(this.ROUTES_URI)
  }
}
