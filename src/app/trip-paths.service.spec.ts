import { TestBed } from '@angular/core/testing';

import { TripPathsService } from './trip-paths.service';

describe('TripPathsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TripPathsService = TestBed.get(TripPathsService);
    expect(service).toBeTruthy();
  });
});
