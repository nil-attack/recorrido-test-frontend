import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TripPathsService {

  TRIP_PATHS_URL = 'http://0.0.0.0/api/v1/trip_paths';

  constructor(private http: HttpClient) {

  }


  getEntranceAndExitRouteEvents(trip_id) {
    const url = `${this.TRIP_PATHS_URL}/events`;
    return this.http.get(url, { params: { trip_id } })
  }

  getByCoordinates(trip_id, latitude, longitude) {
    const url = `${this.TRIP_PATHS_URL}/search`;
    return this.http.get(url, { params: { trip_id, latitude, longitude } })
  }
}
