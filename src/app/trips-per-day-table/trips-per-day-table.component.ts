import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { TripsPerDayService } from "../trips-per-day.service";
import { MatDialog } from "@angular/material/dialog";
import { TripDetailsComponent } from "../trip-details/trip-details.component";
import { map } from "rxjs/operators";

@Component({
  selector: 'app-trips-per-day-table',
  templateUrl: './trips-per-day-table.component.html',
  styleUrls: ['./trips-per-day-table.component.scss']
})
export class TripsPerDayTableComponent implements OnInit {
  trips;
  displayedColumns: string[] = ['patente', 'started_at', 'finished_at', 'status'];

  constructor(public dialogRef: MatDialogRef<TripDetailsComponent>,
              private tripsPerDayService: TripsPerDayService,
              public dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    const {date, route_id, license_plate } = data;
    this.trips = this.tripsPerDayService
                     .getTrips(date, route_id, license_plate)
                     .pipe(map(resp => {
                       const trips = resp["trips"].map(tr => {
                         let newStatus;
                         switch (tr["status"]) {
                           case 'completed':
                             newStatus = 'Completado';
                             break;
                           case 'on route':
                             newStatus = 'En ruta';
                             break;
                           case 'incomplete':
                             newStatus = 'Incompleto';
                             break;
                           default:
                             newStatus = tr["status"]
                         }
                         return Object.assign({}, tr, { status: newStatus })
                       });
                       return {trips}
                     }))
  }

  ngOnInit() {
  }

  onClickRow(trip_id) {
    this.openDialog(trip_id)
  }


  openDialog(trip_id): void {

    const dialogRef = this.dialog.open(TripDetailsComponent, {
      width: '40%',
      data: {trip_id}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
