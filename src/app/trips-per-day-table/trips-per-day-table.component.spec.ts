import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TripsPerDayTableComponent } from './trips-per-day-table.component';

describe('TripsPerDayTableComponent', () => {
  let component: TripsPerDayTableComponent;
  let fixture: ComponentFixture<TripsPerDayTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TripsPerDayTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TripsPerDayTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
