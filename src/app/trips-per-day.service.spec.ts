import { TestBed } from '@angular/core/testing';

import { TripsPerDayService } from './trips-per-day.service';

describe('TripsPerDayService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TripsPerDayService = TestBed.get(TripsPerDayService);
    expect(service).toBeTruthy();
  });
});
