import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TripsPerDayService {

  TRIPS_PER_DAY_URL = 'http://0.0.0.0/api/v1/trips/search'
  constructor(public http: HttpClient) {

  }

  getTrips(date, route_id, license_plate?) {
    return this.http.get(this.TRIPS_PER_DAY_URL, {params: {route_id, date, license_plate}})
  }


}
