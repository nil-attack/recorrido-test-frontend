import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatNativeDateModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button'
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";
import { ReactiveFormsModule } from "@angular/forms";
import { MatRadioModule } from "@angular/material/radio";
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatCardModule } from "@angular/material/card";
import { TripsPerDayTableComponent } from './trips-per-day-table/trips-per-day-table.component';
import { MatTableModule } from "@angular/material/table";
import { TripDetailsComponent } from './trip-details/trip-details.component';
import { DatePipe } from "@angular/common";
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule } from "@angular/material-moment-adapter";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    TripsPerDayTableComponent,
    TripDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule,
    MatRadioModule,
    HttpClientModule,
    MatCardModule,
    MatTableModule

  ],
  entryComponents:[TripsPerDayTableComponent,TripDetailsComponent],
  providers: [HttpClient,
              DatePipe,
              {provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: {useUtc: true}}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
