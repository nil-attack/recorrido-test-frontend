import { Component, OnInit } from '@angular/core';
import { FormBuilder,  Validators } from "@angular/forms";
import { RoutesService } from "../routes.service";
import { MatDialog } from "@angular/material/dialog";
import { TripsPerDayTableComponent } from "../trips-per-day-table/trips-per-day-table.component";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  tripsForm;
  routes;
  constructor(private formBuilder: FormBuilder, private routesService: RoutesService, public dialog:MatDialog) {
    this.createForm()
    this.routes = this.routesService.getAll()
  }

  ngOnInit() {
  }
  createForm() {
    this.tripsForm = this.formBuilder.group({
      route_id: ['', Validators.required],
      date: ['', Validators.required],
      license_plate: [null]
    });
  }

  onSubmit() {
    this.openDialog(this.tripsForm.value);
  }
  openDialog(formValues): void {
    const { route_id, date, license_plate } = formValues

    const apiFormattedDate = date.split("-").reverse().join("-")
    const dialogRef = this.dialog.open(TripsPerDayTableComponent, {
      width: '40%',
      data: {route_id, date: apiFormattedDate, license_plate}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


  date(e) {
    let convertDate = new Date(e.target.value).toISOString().substring(0, 10);
    this.tripsForm.get('date').setValue(convertDate, {
      onlyself: true
    })
  }

}
