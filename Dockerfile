FROM node:12.14.1-buster
RUN apt-get update && apt-get install -y build-essential inotify-tools
RUN npm install -g @angular/cli
RUN mkdir -p /app
WORKDIR /app
ADD . ./
EXPOSE 4200
